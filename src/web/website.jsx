import React, { Component } from "react";
import "./website.scss";
import web from "../image/web.jpg";
import tpic from "../image/tt.png";
import gpic from "../image/Rocket.png";
import WebTypes from "./webtypes";

class WebSite extends Component {
  render() {
    return (
      <div className="webPage">
        <h1>WEBSITE DEVELOPMENT SERVICES</h1>
        <div className="web-flex">
          <div className="web-left">
            <img className="web-pic" src={web} />
          </div>
          <div className="web-right">
            <p>
              Creation of websites from the web-studio The SEO starts with
              analytics
            </p>
            <div className="web-details">
              <i class="fa fa-lightbulb-o" />
              <ul>
                <li>
                  <h6>Analysis</h6>
                </li>
                <li>
                  <p>First we think, then we work</p>
                </li>
              </ul>
            </div>
            <div className="web-details">
              <i class="fa fa-thumbs-up" />
              <ul>
                <li>
                  <h6>Reports</h6>
                </li>
                <li>
                  <p>We agree on all stages of creation</p>
                </li>
              </ul>
            </div>
            <div className="web-details">
              <i class="fa fa-hourglass-half" />
              <ul>
                <li>
                  <h6>Deadline</h6>
                </li>
                <li>
                  <p>We pass the job on the time</p>
                </li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary">
              Order Website for Sale
            </button>
          </div>
        </div>
        <WebTypes />
        {/* <div className="m-benefit">
          <h2>BENEFITS</h2>
        </div> */}
        <div className="web-footer">
          <img className="wm-pic" src={tpic} />
          <img className="wm1-pic" src={gpic} />
          <div className="web-absolute">
            <h6>
              Don’t have a website yet? Discover what you are missing. Got a
              website already? Give it a new look
            </h6>
            <h1>Find Out What Your Website Can Do For Your Business.</h1>
            <div className="footer-flex">
              <div className="l-footer">
                <h4>FREE CONSULTATION</h4>
                <form>
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Name"
                  />
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="E-mail"
                  />
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Phone Number"
                  />
                  <div className="form-group">
                    <textarea
                      className="form-control form-control-lg"
                      id="exampleFormControlTextarea1"
                      rows="3"
                      placeholder="Message"
                    />
                    <button type="button" class="btn btn-primary">
                      Send Request
                    </button>
                  </div>
                </form>
              </div>
              <div className="r-footer">
                <h4>LET'S DISCUSS YOUR PROJECT</h4>
                <p>
                  You will receive a brief free consultation from one of our
                  leading marketers. The consultation includes:
                </p>
                <ul>
                  <li>
                    General errors of your site in terms of marketing, usability
                    and design
                  </li>
                  <li>
                    Recommendations of changes, A/B testing and selection of
                    strategies to increase the conversion of site
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default WebSite;
