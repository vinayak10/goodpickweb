import React, { Component } from "react";
import "./website.scss";
import show1 from "../image/wd.jpg";
import show2 from "../image/wd3.jpg";
import show3 from "../image/gd1.png";
import show4 from "../image/ecom.jpeg";
import show5 from "../image/mapp.png";
import show6 from "../image/webo.jpg";
// import  from "../image/";
// import  from "../image/";
// import  from "../image/";

const types = [
  {
    img: show1,
    title: "Website Design",
    para:
      "Let’s create a personalized website for your business, brand, event or any reason you need it for."
  },
  {
    img: show2,
    title: "Web Development",
    para:
      "Get a clean, lean, fast and Google-friendly website for your business."
  },
  {
    img: show3,
    title: "Graphic Design",
    para:
      "Your audience is a visual leaner. Feed it with the right graphic content."
  },
  {
    img: show4,
    title: "E-commerce",
    para:
      "Get your retail store online and make shopping an entertainment for your shoppers."
  },

  {
    img: show5,
    title: "Mobile Apps Development",
    para:
      "Engage your customers beyond just desktop with highly interactive mobile apps."
  },
  {
    img: show6,
    title: "Web Presence Optimization",
    para: "Close to 80% of purchase-related research is conducted online."
  }
];

class WebTypes extends Component {
  render() {
    return (
      <div className="Web-type">
        <div className="wm-absolute">
          <h1>Types Of Website Services</h1>
          <div className="card-view">
            {types.map(items => (
              <div key={items.name1} className="card mb-3">
                <img className="card-img-top" src={items.img} />
                <h5 className="card-title">{items.title}</h5>
                <div className="card-overlay">
                  <div className="text-tab">
                    <h6 className="card-title">{items.title}</h6>
                    <p className="card-text">{items.para}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
export default WebTypes;
