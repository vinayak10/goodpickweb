import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import Content from "./content/content";
// import Footer from "./mainpage/footer/footer";

class ContentPage extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <Content />
        {/*<Footer /> */}
      </div>
    );
  }
}
export default ContentPage;
