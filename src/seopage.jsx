import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import Seo from "./seo/seo";
import Footer from "./mainpage/footer/footer";

class SeoPage extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <Seo />
        <Footer />
      </div>
    );
  }
}
export default SeoPage;
