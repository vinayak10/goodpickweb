import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import ServicePage from "./service/servicepage";
import Footer from "./mainpage/footer/footer";

class Service extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <ServicePage />
        <Footer />
      </div>
    );
  }
}
export default Service;
