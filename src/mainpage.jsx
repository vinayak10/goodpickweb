import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import CoverPage from "./mainpage/coverpage/coverpage";
import Footer from "./mainpage/footer/footer";

class Mainpage extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <CoverPage />
        <Footer />
      </div>
    );
  }
}
export default Mainpage;
