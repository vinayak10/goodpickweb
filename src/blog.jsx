import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
// import ServicePage from "./blog/blog";
import Footer from "./mainpage/footer/footer";

class Blog extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        {/* <ServicePage /> */}
        <Footer />
      </div>
    );
  }
}
export default Blog;
