import React, { Component } from "react";
import "./seo.scss";
import pic1 from "../image/seo1.png";
import pic2 from "../image/seo2.png";
import pic3 from "../image/seo3.png";
import pic4 from "../image/seo4.png";
import pic5 from "../image/seo5.png";
import pic6 from "../image/seo6.png";
import pic7 from "../image/seo7.png";
import pic8 from "../image/seo8.png";
import pic9 from "../image/seo9.png";
import pic10 from "../image/seo10.png";
import pic11 from "../image/seo11.png";
import pic12 from "../image/seo12.png";
import pic13 from "../image/seo13.png";
import pic14 from "../image/seo14.png";
import pic15 from "../image/seo15.png";
import pic16 from "../image/seo16.png";

const stypes = [
  {
    img: pic1,
    title: "Local SEO",
    para:
      "Attract all the customers in the neighborhood with a micro-targeted local SEO plan."
  },
  {
    img: pic2,
    title: "National SEO",
    para:
      "Convert your local mom n’ pop shop into a national success with the help of national SEO."
  },
  {
    img: pic3,
    title: "Franchise SEO",
    para:
      "Make each of your franchise equally profitable by leveraging the power of the search."
  },
  {
    img: pic4,
    title: "E-commerce SEO",
    para:
      "Become their trusted store and track their activities for better remarketing opportunities."
  },
  {
    img: pic5,
    title: "White Label SEO",
    para:
      "Add scalability to your business by joining hands with the #1 SEO company."
  },
  {
    img: pic6,
    title: "Mobile SEO",
    para:
      "Get a custom SEO strategy for your mobile website and earn traffic on-the-go."
  },
  {
    img: pic7,
    title: "App Store SEO",
    para: "Get more downloads by improving your mobile app’s visibility."
  },
  {
    img: pic8,
    title: "SEO Ranking Recovery",
    para: "Recover and protect your website by making it Google updates-proof."
  },
  {
    img: pic9,
    title: "SEO by Industry",
    para: "Every industry functions differently. So are their SEO practices."
  },
  {
    img: pic10,
    title: "SEO by Location",
    para:
      "Why have a generic strategy when you can have an SEO plan made for your location?"
  },
  {
    img: pic11,
    title: "Quick SEO Audit",
    para: "A 360-degree SEO audit followed by expert insights"
  },
  {
    img: pic12,
    title: "Conversion Rate Optimisation",
    para:
      "At the risk of sounding exaggerating, almost every business website faces a conversion issue."
  },
  {
    img: pic13,
    title: "Infographics",
    para:
      "A study shows that websites that use Infographics for promoting the content receive over 10% greater traffic than those that don't"
  },
  {
    img: pic14,
    title: "Video Distribution",
    para:
      "About 7 in every 10 users are likely to visit your website after viewing your video."
  },
  {
    img: pic15,
    title: "Multilingual SEO",
    para: "It's about time you took your website multinational."
  },
  {
    img: pic16,
    title: "Enterprise SEO",
    para:
      "As per a recently conducted study, over half of the companies that struggle with their enterprise SEO"
  }
];

class SeoTypes extends Component {
  render() {
    return (
      <div className="s-page">
        <h1>Types Of SEO Services</h1>
        <div className="card-view">
          {stypes.map(items => (
            <div key={items.name1} className="card mb-3">
              <img className="card-img-top" src={items.img} />
              <h5 className="card-title">{items.title}</h5>
              <div className="card-overlay">
                <div className="text-tab">
                  <h6 className="card-title">{items.title}</h6>
                  <p className="card-text">{items.para}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
export default SeoTypes;
