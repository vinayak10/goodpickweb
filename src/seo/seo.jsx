import React, { Component } from "react";
import "./seo.scss";
import img1 from "../image/first.png";
import SeoTypes from "./seotypes";

class Seo extends Component {
  render() {
    return (
      <div className="seo-page">
        <h1>LOCAL SEARCH ENGINE OPTIMIZATION SERVICES</h1>
        <div className="s-header">
          <div className="sh-left">
            <img className="image1" src={img1} />
          </div>
          <div className="sh-right">
            <p>
              We provide high quality services of Local Search Engine
              Optimization. It’s include:
            </p>
            <ul>
              <li>Directory submission and optimization services</li>
              <li>Organic SEO results</li>
              <li>Landing page promotion</li>
              <li>Google Maps</li>
              <li>Local Newspaper and Communities</li>
              <li>Innovative yet most affordable SEO packages</li>
              <li>Google Analytics Certified Search Engine Optimization</li>
              <li>Analytics-driven high-ROI SEO campaigns</li>
            </ul>
          </div>
        </div>
        <SeoTypes />
        <div className="s-footer">
          <h1>Send Message</h1>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="Name"
                />
              </div>
              <div class="form-group col-md-6">
                <input
                  type="password"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Website URL"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="E-mail"
                />
              </div>
              <div class="form-group col-md-6">
                <input
                  type="password"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Phone Number"
                />
              </div>
            </div>
            <div className="form-group">
              <textarea
                className="form-control msg"
                rows="5"
                id="comment"
                name="text"
                placeholder="Your Message"
              />
            </div>
            <button type="button" class="btn btn-light">
              Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default Seo;
