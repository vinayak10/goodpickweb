import React, { Component } from "react";
import "./coverpage.scss";
import pic from "../../image/footer.png";
import FooterEnd from "./footer-end";
// import  from "..//";

class Footer extends Component {
  render() {
    return (
      <div className="footerPage">
        <h1>Reasons Your Business Needs Us</h1>
        <div className="f-page">
          <div className="f-para">
            <div className="first-para">
              <h6>Can you afford to miss out on this opportunity?</h6>
              <p>
                Only 3 in 10 businesses in your zip code have claimed their
                business listing on search.
              </p>
            </div>
            <div className="s-para">
              <h6>Let’s give your marketing the power of analytics.</h6>
              <p>
                Hardly 10% of are somewhat confident that their marketing
                intelligence data are well integrated.
              </p>
            </div>
            <div className="t-para">
              <h6>Reach them right when they are actively searching.</h6>
              <p>Roughly 90% of your online customers have a mobile.</p>
            </div>
          </div>
          <div className="f-pic">
            <img className="" src={pic} />
          </div>
          <div className="f-para">
            <div className="first-para">
              <h6>Boost your bottom line with social media.</h6>
              <p>
                Every new like on your Facebook page is a potential business
                worth $214.81.
              </p>
            </div>
            <div className="s-para">
              <h6>Wait no more. Say “I do” to digital marketing.</h6>
              <p>
                The cost of a wrong digital campaign is only one fifth the cost
                of not trying at all.
              </p>
            </div>
            <div className="t-para">
              <h6>
                Don’t just engage, own the word-of-mouth on the web about you.
              </h6>
              <p>
                Each fourth new customer out of five has read an online review
                about your business.
              </p>
            </div>
          </div>
        </div>
        <div className="f-block">
          <div className="left">
            <h2>DO YOU WANT TO GET RESULTS</h2>
            <p>from your new website?</p>
          </div>
          <div className="right">
            <button type="button" class="btn btn-light">
              Get Started
            </button>
          </div>
        </div>
        <FooterEnd />
      </div>
    );
  }
}
export default Footer;
