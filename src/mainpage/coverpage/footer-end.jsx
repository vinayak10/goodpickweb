import React, { Component } from "react";
import "./footer-end.scss";
import img1 from "../../image/f1.png";
import img2 from "../../image/f2.png";
import img3 from "../../image/f3.png";
import img4 from "../../image/f4.png";
import img5 from "../../image/f5.png";
// import  from "..//";

class FooterEnd extends Component {
  render() {
    return (
      <div className="footer-end">
        <div className="first">
          <div className="first-block">
            <img className="fb1" src={img1} />
            <p>GLOBAL REACH</p>
          </div>
          <div className="second-block">
            <img className="fb2" src={img2} />
            <p>BEST PRICES</p>
          </div>
        </div>
        <div className="second">
          <h4>WHY US?</h4>
          <p>
            We offer synergistic online products, social media marketing and
            dynamic SEO strategies deliverin.
          </p>
        </div>
        <div className="first">
          <div className="first-block">
            <img className="fb3" src={img3} />
            <p>GLOBAL REACH</p>
          </div>

          <div className="second-block">
            <img className="fb4" src={img5} />
            <p>BEST PRICES</p>
          </div>
        </div>
      </div>
    );
  }
}
export default FooterEnd;
