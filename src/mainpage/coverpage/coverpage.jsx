import React, { Component } from "react";
import "./coverpage.scss";
import Footer from "./footer.jsx";
import pic1 from "../../image/logo1.png";
import pic2 from "../../image/logo2.png";
import pic3 from "../../image/logo3.png";
import pic4 from "../../image/logo4.png";
import pic5 from "../../image/m-pic.gif";
import pic6 from "../../image/mpic.gif";
import pic7 from "../../image/mpic1.gif";
import pic8 from "../../image/photo.jpg";

const value = [
  {
    img: pic1,
    title: "WEBSITE DEVELOPMENT",
    para:
      "We create websites and applications that provide effective solutions for your goals"
  },
  {
    img: pic2,
    title: "SEARCH ENGINE MARKET",
    para:
      "Complex strategies with link building, pay-per-click campaigns, content marketing and others"
  },
  {
    img: pic3,
    title: "PAY-PER-CLICK",
    para:
      "Just pay for results. We can give you clients for your business tomorrow!"
  },
  {
    img: pic4,
    title: "SEARCH ENGINE OPTIMIZATION",
    para: "On page optimization and link building strategies"
  }
];

class CoverPage extends Component {
  render() {
    return (
      <div className="cover-page">
        <h1>OUR SERVICES</h1>
        <div className="c-page">
          {value.map(items => (
            <div key={items.title} className="services">
              <div className="c-pic">
                <img className="pic-logo" src={items.img} />
              </div>
              <h6>{items.title}</h6>
              <p>{items.para}</p>
            </div>
          ))}
        </div>
        <div className="middle">
          <div className="m-left">
            <h1>REAL SUPPORT</h1>
            <h5>
              No chatbots or ghostbots. Real people, real emails and calls, real
              answers.
            </h5>
            <p>
              Reliable, knowledgeable, real-time online marketing campaign
              support.
              <br /> Fast turnaround and accurate responses and recommendations.
              <br /> Unlimited access to digital marketing support resources
              database.
            </p>
          </div>
          <div className="m-right">
            <img className="m-pic" src={pic5} />
          </div>
        </div>
        <div className="middle s-wrap">
          <div className="m-right">
            <img className="m-pic" src={pic6} />
          </div>
          <div className="m-left">
            <h1>REAL IMPACTFUL</h1>
            <h5>Dominate the big 3 – Search, Social and Mobile.</h5>
            <p>
              Your customer’s journey with starts with a powerful insight.
              <br />
              Digital marketing success comes in all sizes and shapes.
              <br />
              Personalized unified experiences, beautifully delivered.
            </p>
          </div>
        </div>
        <div className="middle">
          <div className="m-left">
            <h1>REAL SIMPLE</h1>
            <h5>Customized solutions for the savvy digital marketers</h5>
            <p>
              Automate your core marketing activities.
              <br /> Easy to understand, so you can focus on running your
              business.
              <br /> From “Oh cr*p!” to “Ahaa!” – Digital marketing made simple.
            </p>
          </div>
          <div className="m-right">
            <img className="m-pic" src={pic7} />
          </div>
        </div>
        <div className="middle-c">
          <div className="m-left-c">
            <h2>About Us</h2>
            <img className="m-pic-c" src={pic8} />
          </div>
          <div className="m-right-c">
            <div className="accordion" id="accordionExample">
              <div className="card">
                <div className="card-header" id="headingOne">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      <i className="fa fa-user" />
                      Who are We
                    </button>
                  </h5>
                </div>

                <div
                  id="collapseOne"
                  className="collapse show"
                  aria-labelledby="headingOne"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    We are a full-service SEO agency. Our social media experts
                    can help you establish your business objectives, identify
                    your target audience, create engaging and share-worthy
                    content, and finally integrate your social media with all
                    other aspects of your online presence.
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingTwo">
                  <h5 className="mb-0">
                    <button
                      className="btn btn-link collapsed"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      <i className="fa fa-cog" />
                      What Do We Do
                    </button>
                  </h5>
                </div>
                <div
                  id="collapseTwo"
                  className="collapse"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    I am text block. Click edit button to change this text.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus
                    leo.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
export default CoverPage;
