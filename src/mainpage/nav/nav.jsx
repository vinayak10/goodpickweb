import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./nav.scss";
import Header from "./header";

class Nav extends Component {
  render() {
    return (
      <div className="nav-bar">
        <nav id="navbar" className="navbar navbar-expand-lg navbar-light">
          <a className="navbar-brand my-lg-0" href="#">
            GoodPickWeb
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  <Link to="/" className="link-page">
                    Home
                  </Link>
                  <span className="sr-only">(current)</span>
                </a>
              </li>
              {/* <li className="nav-item">
                <a className="nav-link" href="#">
                  <Link to="/feature" className="link-page">
                    Features
                  </Link>
                </a>
              </li> */}
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Services
                </a>
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <a className="dropdown-item" href="#">
                    <Link to="/web" className="link-page">
                      Web Development
                    </Link>
                  </a>
                  <a className="dropdown-item" href="#">
                    <Link to="/seopage" className="link-page">
                      SEO{" "}
                    </Link>
                  </a>
                  <a className="dropdown-item" href="#">
                    <Link to="/ppcpage" className="link-page">
                      Pay per Click
                    </Link>
                  </a>
                  <a className="dropdown-item" href="#">
                    <Link to="/digipage" className="link-page">
                      Digital Marketing
                    </Link>
                  </a>
                  <a className="dropdown-item" href="#">
                    <Link to="/contentpage" className="link-page">
                      Content Marketing
                    </Link>
                  </a>
                  <a className="dropdown-item" href="#">
                    <Link to="/ppcpage" className="link-page">
                      Digital Marketing
                    </Link>
                  </a>
                </div>
              </li>
              {/* <li className="nav-item">
                <a className="nav-link" href="#">
                  <Link to="/blog" className="link-page">
                    Blog
                  </Link>
                </a>
              </li> */}

              <li className="nav-item">
                <a className="nav-link" href="#">
                  <Link to="/contact" className="link-page">
                    Contacts
                  </Link>
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <Header />
      </div>
    );
  }
}
export default Nav;
