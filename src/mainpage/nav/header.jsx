import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./nav.scss";
import pic from "../../image/header.png";

class Header extends Component {
  render() {
    return (
      <div className="header-page">
        <img className="h-pic" src={pic} />
        <div className="overlay-h">
          <h1>
            Consistently Ranked Amongst the Best Digital Marketing, SEO & Web
            Design Companies in India.
          </h1>
          <div className="btn-flex">
            <button class="btn">
              <Link to="/service" className="link-page">
                Our Services
              </Link>
            </button>
            <button class="btn">
              <Link to="/contact" className="link-page">
                Contact Us
              </Link>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Header;
