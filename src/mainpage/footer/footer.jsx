import React, { Component } from "react";
import "./footer.scss";
// import  from "../../image/";
// import  from "..//";

class Footer extends Component {
  render() {
    return (
      <div className="footer-page">
        <div className="l-side">
          <div className="title">
            <h1>GoodPickWeb</h1>
            <div className="social">
              <i className="fa fa-facebook" />
              <i className="fa fa-twitter" />
              <i className="fa fa-linkedin" />
              <i className="fa fa-google-plus" />
            </div>
          </div>
          <h6>Grow Your Online Presence</h6>
          <p>Social Media Marketing and Optimization Agency</p>
        </div>
        <div className="r-side">
          <div className="leftside">
            <h4>Quick Links</h4>
            <p>
              <i class="fa fa-home" /> Home
            </p>
            <p>
              <i class="fa fa-cogs" /> Services
            </p>
            <p>
              <i class="fa fa-cart-arrow-down" /> SMM and SMO Services
            </p>
            <p>
              <i class="fa fa-inr" /> Price
            </p>
            <p>
              <i class="fa fa-bar-chart" /> Free SEO Report
            </p>
            <p>
              <i class="fa fa-envelope-o" /> Message
            </p>
          </div>
          <div className="rightside">
            <h4>Contact Info</h4>
          </div>
        </div>
      </div>
    );
  }
}
export default Footer;
