import React, { Component } from "react";
import "./ppc.scss";
import picture from "../image/ppc.png";
import pic1 from "../image/google.jpg";
import pic2 from "../image/bing.png";
import pic3 from "../image/fb.png";
import pic4 from "../image/commerce.png";
import pic5 from "../image/linked.png";
import pic6 from "../image/remarketing.jpg";
import pic7 from "../image/media.png";
import pic8 from "../image/ads.png";

const ptypes = [
  {
    img: pic1,
    title: "Google PPC",
    para: "Be found by customers on the planet's biggest search engine."
  },
  {
    img: pic2,
    title: "Bing PPC",
    para:
      "Get your share of the pie with over 161 Million active searchers on Bing."
  },
  {
    img: pic3,
    title: "Facebook PPC",
    para:
      "Drive sales from the place where your customers treat you as a friend."
  },
  {
    img: pic4,
    title: "E-commerce PPC",
    para: "Sell your products directly on the search engine results page."
  },
  {
    img: pic5,
    title: "LinkedIn PPC",
    para: "Find customers from the world’s largest network of professionals."
  },
  {
    img: pic6,
    title: "Remarketing",
    para:
      "Stay on top-of-the-mind for those who visited your website in the past."
  },
  {
    img: pic7,
    title: "Media Buying Services",
    para:
      "More than half of your customers are going omni-channel when searching for your business or products and services."
  },
  {
    img: pic8,
    title: "Google Adwords",
    para:
      "3 out of 5 searchers, on an average, notice a remarketing ad. Plus, they are likely to respond favorably to your ad due to an enhanced brand resonance."
  }
];

class Ppc extends Component {
  render() {
    return (
      <div className="ppc-page">
        <h1>PAY PER CLICK SETUP & MANAGEMENT SERVICES</h1>
        <div className="first">
          <div className="box">
            <i class="fa fa-smile-o" />
            <p>Free conduct an audit of campaigns, niche and website</p>
          </div>
          <div className="box">
            <i class="fa fa-bar-chart" />
            <p>We provide clear reports of the results</p>
          </div>
          <div className="box">
            <i class="fa fa-phone" />
            <p>Increase the number of emails and phone calls</p>
          </div>
          <div className="box">
            <i class="fa fa-angle-double-down" />
            <p>Reduce the average cost of clicks and customers</p>
          </div>
        </div>
        <h1>IN PPC ADVERTISING IS ONLY ONE GOAL - TO SELL</h1>
        <div className="second">
          <div className="left">
            <img className="pic" src={picture} />
          </div>
          <div className="right">
            <p>Some SEO text about benefits PPC advertising.</p>
            <p>
              Pay-per-click, along with cost per impression and cost per order,
              are used to assess the cost effectiveness and profitability of
              internet marketing. Pay-per-click has an advantage over cost per
              impression in that it tells us something about how effective the
              advertising was.
            </p>
            <p>
              Clicks are a way to measure attention and interest. If the main
              purpose of an ad is to generate a click, or more specifically
              drive traffic to a destination, then pay-per-click is the
              preferred metric.
            </p>
          </div>
        </div>
        <h1>Types Of PPC Services</h1>
        <div className="ppcTypes">
          <div className="card-view">
            {ptypes.map(items => (
              <div key={items.name1} className="card mb-3">
                <img className="card-img-top" src={items.img} />
                <h5 className="card-title">{items.title}</h5>
                <div className="card-overlay">
                  <div className="text-tab">
                    <h6 className="card-title">{items.title}</h6>
                    <p className="card-text">{items.para}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <h1>Send Message</h1>
        <form>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input
                type="email"
                class="form-control"
                id="inputEmail4"
                placeholder="Name"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                type="password"
                class="form-control"
                id="inputPassword4"
                placeholder="Website URL"
              />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input
                type="email"
                class="form-control"
                id="inputEmail4"
                placeholder="E-mail"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                type="password"
                class="form-control"
                id="inputPassword4"
                placeholder="Phone Number"
              />
            </div>
          </div>
          <div className="form-group">
            <textarea
              className="form-control msg"
              rows="5"
              id="comment"
              name="text"
              placeholder="Your Message"
            />
          </div>
          <button type="button" class="btn btn-light">
            Submit
          </button>
        </form>
      </div>
    );
  }
}
export default Ppc;
