import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import WebSite from "./web/website";
import Footer from "./mainpage/footer/footer";

class Web extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <WebSite />
        <Footer />
      </div>
    );
  }
}
export default Web;
