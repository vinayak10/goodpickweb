import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import Ppc from "./ppc/ppc";
// import Footer from "./mainpage/footer/footer";

class PpcPage extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <Ppc />
        {/* <Footer /> */}
      </div>
    );
  }
}
export default PpcPage;
