import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Route, Switch, Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import './index.css';
import Mainpage from './mainpage';
import Service from './service';
import Contact from './contact';
import Blog from './blog';
import Web from './web';
import SeoPage from './seopage';
import PpcPage from './ppcpage';
import DigiPage from './digipage';
import ContentPage from './contentpage';


const history = createBrowserHistory();
ReactDOM.render(
    <div>
        <Router history={history}>
            <div>
                <Switch>
                    <Route exact path="/" component={Mainpage} />
                    <Route exact path="/service" component={Service} />
                    <Route exact path="/contact" component={Contact} />
                    <Route exact path="/blog" component={Blog} />
                    <Route exact path="/web" component={Web} />
                    <Route exact path="/seopage" component={SeoPage} />
                    <Route exact path="/ppcpage" component={PpcPage} />
                    <Route exact path="/digipage" component={DigiPage} />
                    <Route exact path="/contentpage" component={ContentPage} />
                </Switch>
            </div>
        </Router>
    </div>
    , document.getElementById('root')
);


serviceWorker.unregister();
