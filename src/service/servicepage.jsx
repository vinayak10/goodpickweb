import React, { Component } from "react";
import "./servicepage.scss";
// import  from "..//";
import logo1 from "../image/logo1.png";
import logo2 from "../image/logo2.png";
import logo3 from "../image/logo3.png";
import logo4 from "../image/logo4.png";
import logo5 from "../image/logo5.png";
import logo6 from "../image/logo6.png";
import logo7 from "../image/logo7.png";
import logo8 from "../image/logo8.png";

const card = [
  {
    img: logo1,
    title: "WEBSITE DEVELOPMENT",
    para:
      "We create websites and applications that provide effective solutions for your goals"
  },
  {
    img: logo2,
    title: "SEARCH ENGINE MARKET",
    para:
      "Complex strategies with link building, pay-per-click campaigns, content marketing and others"
  },
  {
    img: logo3,
    title: "PAY-PER-CLICK",
    para:
      "Just pay for results. We can give you clients for your business tomorrow!"
  },
  {
    img: logo4,
    title: "SEARCH ENGINE OPTIMIZATION",
    para: "On page optimization and link building strategies"
  },
  {
    img: logo5,
    title: "CONTENT MARKETING",
    para:
      "One of the most effective and long-term tools to date to enhance loyalty"
  },
  {
    img: logo6,
    title: "LOCAL SEO",
    para:
      "Directory submission, online reputation management and optimization services for your region"
  },
  {
    img: logo7,
    title: "E-MAIL MARKETING",
    para: "Letters and letter chains that sell your services and products"
  },
  {
    img: logo8,
    title: "CONVERSION RATE OPTIMIZATION",
    para: "Work with your Call-to-Actions and Usability"
  }
];

class ServicePage extends Component {
  render() {
    return (
      <div className="s-page">
        <h1>OUR SERVICES</h1>
        <div className="card-view">
          {card.map(items => (
            <div key={items.name1} className="card mb-3">
              <img className="card-img-top" src={items.img} />
              <h5 className="card-title">{items.title}</h5>
              <div className="card-overlay">
                <div className="text-tab">
                  <h6 className="card-title">{items.title}</h6>
                  <p className="card-text">{items.para}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
export default ServicePage;
