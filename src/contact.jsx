import React, { Component } from "react";
import Nav from "./mainpage/nav/nav";
import ContactPage from "./contact/contactpage";
import Footer from "./mainpage/footer/footer";

class Contact extends Component {
  render() {
    return (
      <div className="frontPage">
        <Nav />
        <ContactPage />
        <Footer />
      </div>
    );
  }
}
export default Contact;
