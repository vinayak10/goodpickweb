import React, { Component } from "react";
import "./contactpage.scss";
import Choose from "./choose";
import point from "../image/pointing.png";
import back from "../image/back.png";

class ContactPage extends Component {
  render() {
    return (
      <div className="contact-page">
        <h1>CONTACT US</h1>
        <div className="c-middle">
          <div className="cm-left">
            <p>
              We appreciate your business and guarantee a response within 24
              Business hours which would include our Packages and Pricing.
            </p>
            <div className="line">
              <i class="fa fa-skype" />
              <h6>Skype:</h6>
              <p>webpick</p>
            </div>
            <div className="line">
              <i class="fa fa-envelope" />
              <h6>E-mail:</h6>
              <p>webpick</p>
            </div>
            <div className="line">
              <i class="fa fa-address-card" />
              <h6>Address:</h6>
              <p>webpick</p>
            </div>
            <div className="line">
              <i class="fa fa-phone" />
              <h6>Phone no:</h6>
              <p>webpick</p>
            </div>
            <div className="line">
              <i class="fa fa-clock-o" />
              <h6>Timing:</h6>
              <p>webpick</p>
            </div>
          </div>
          <div className="cm-right">
            <img className="point" src={point} />
          </div>
        </div>
        <div className="c-footer">
          <h1>Send Message</h1>
          <form>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="Name"
                />
              </div>
              <div class="form-group col-md-6">
                <input
                  type="password"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Website URL"
                />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder="E-mail"
                />
              </div>
              <div class="form-group col-md-6">
                <input
                  type="password"
                  class="form-control"
                  id="inputPassword4"
                  placeholder="Phone Number"
                />
              </div>
            </div>
            <div className="form-group">
              <textarea
                className="form-control msg"
                rows="5"
                id="comment"
                name="text"
                placeholder="Your Message"
              />
            </div>
            <Choose />
            <button type="button" class="btn btn-light">
              Submit
            </button>
          </form>
          <img className="back-pic" src={back} />
        </div>
      </div>
    );
  }
}
export default ContactPage;
