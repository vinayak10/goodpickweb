import React, { Component } from "react";
import "./content.scss";
import pic from "../image/sb.png";
import pic1 from "../image/cm1.png";
import pic2 from "../image/cm2.png";
import pic3 from "../image/cm3.png";
import pic4 from "../image/cm4.png";
import pic5 from "../image/cm 5.png";
import pic6 from "../image/cm6.png";
import pic7 from "../image/cm7.png";
import pic8 from "../image/cm8.png";
import pic9 from "../image/cm9.png";

const ctypes = [
  {
    img: pic1,
    title: "SEO-friendly web content",
    para:
      "High-quality content that attracts users and also the search engine bots."
  },
  {
    img: pic2,
    title: "Blog posts",
    para:
      "Regularly publish highly engaging posts on your site and build a follower base."
  },
  {
    img: pic3,
    title: "Social media content",
    para:
      "Unique content for each social media platform to generate higher referral traffic."
  },
  {
    img: pic4,
    title: "Press releases",
    para:
      "Get the attention of A-rated journalists with sticky press release articles."
  },
  {
    img: pic5,
    title: "Infographics",
    para:
      "Convert your boring textual content into attractive infographics to get more clicks."
  },
  {
    img: pic6,
    title: "Videos",
    para:
      "Your customers are visual learners. Feed them with great story-like videos."
  },
  {
    img: pic7,
    title: "Newsletters",
    para:
      "Keep a constant touch with your customers and partners and increase your sales."
  },
  {
    img: pic8,
    title: "Case studies",
    para:
      "Persuasively present the customer pain points and win them over with solutions."
  },
  {
    img: pic9,
    title: "Content Marketing",
    para:
      "As suggested by a recent industry study, the B2B websites that have quality and user-friendly content generate 67% higher leads than those without one."
  }
];

class Content extends Component {
  render() {
    return (
      <div className="c-page">
        <div className="c-header">
          <h1>CONTENT MARKETING SERVICES</h1>
          <div className="content">
            <ul>
              <li>
                <h6>Posts to Instagram and FaceBook</h6>
                <p>
                  In social networks, you can make great-targeted campaigns in
                  the film (in the form of creative posts with pictures and
                  witty text)
                </p>
              </li>
              <li>
                <h6>Integration in the YouTube videos-bloggers</h6>
                <p>
                  This is the new must-have in any media plan for any business
                  of any subject. YouTube – one of the main suppliers of traffic
                  in internet
                </p>
              </li>
              <li>
                <h6>Reviews in communities</h6>
                <p>
                  Communities where full of life and every post is gaining
                  thousands and tens of thousands of real views and hundreds of
                  comments
                </p>
              </li>
              <li>
                <h6>Posts from top bloggers</h6>
                <p>
                  Unfortunately, effective bloggers are not so much. Therefore,
                  we try to include in the media plan leading bloggers, who have
                  proven themselves in a large audience
                </p>
              </li>
              <li>
                <h6>Niche websites</h6>
                <p>
                  In almost every category has at least a few niche sites where
                  is concentrated core of the target audience. There is no
                  attendance million, but collected all the people who should
                  know about your product/service in the first place.
                </p>
              </li>
              <li>
                <h6>Publications in mass media</h6>
                <p>
                  This channel is very expensive and it is more fashion than
                  targeted traffic and sales. But if you have serious plans and
                  significant information occasions, we will make for you a
                  series of publications in the media.
                </p>
              </li>
            </ul>
          </div>
        </div>
        <h1>Types Of Content Marketing</h1>
        <div className="c-types">
          <div className="card-view">
            {ctypes.map(items => (
              <div key={items.name1} className="card mb-3">
                <img className="card-img-top" src={items.img} />
                <h5 className="card-title">{items.title}</h5>
                <div className="card-overlay">
                  <div className="text-tab">
                    <h6 className="card-title">{items.title}</h6>
                    <p className="card-text">{items.para}</p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="c-footer">
          <img className="back" src={pic} />
          <div className="c-overlay">
            <h1>GOALS OF CONTENT MARKETING</h1>
            <ul>
              <li>
                <i class="fa fa-users" />
                <h5>Formation of brand</h5>
                <p>
                  Convey your benefits and value to your audience. Attract new
                  subscribers to the channels of communication and transforms
                  them into brand advocates by regular, useful and expert
                  content.
                </p>
              </li>
              <li>
                <i class="fa fa-pie-chart" />
                <h5>Increase of sales</h5>
                <p>
                  Engage potential customers and tell us about promotions and
                  special offers. Expand the benefits of new products.
                </p>
              </li>
              <li>
                <i class="fa fa-pagelines" />
                <h5>Promoting of content</h5>
                <p>
                  Create a strong content that will be shared on social
                  networks. Promote the content in social networks on the target
                  audience to get more coverage. Keep a blog to attract new
                  potential customers from search engines.
                </p>
              </li>
            </ul>
          </div>
        </div>
        <h1>Send Message</h1>
        <form>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input
                type="email"
                class="form-control"
                id="inputEmail4"
                placeholder="Name"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                type="password"
                class="form-control"
                id="inputPassword4"
                placeholder="Website URL"
              />
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <input
                type="email"
                class="form-control"
                id="inputEmail4"
                placeholder="E-mail"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                type="password"
                class="form-control"
                id="inputPassword4"
                placeholder="Phone Number"
              />
            </div>
          </div>
          <div className="form-group">
            <textarea
              className="form-control msg"
              rows="5"
              id="comment"
              name="text"
              placeholder="Your Message"
            />
          </div>
          <button type="button" class="btn btn-light">
            Submit
          </button>
        </form>
      </div>
    );
  }
}
export default Content;
